package endpoint;

import engine.Market;
import engine.Order;
import engine.OrderBook;
import engine.Side;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import util.RESTUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class MainEndpoint extends HttpServlet {
    Logger logger = Logger.getLogger(MainEndpoint.class.getSimpleName());
    static Market market = new Market();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String content = RESTUtil.readStringInput(req.getInputStream());
        logger.info("Posted Content: " + content);
        String command = req.getParameter("command");
        JSONObject jsonObject = null;
        logger.info("Content: " + content);
        String pair = null;
        long price, quantity, orderId;
        if (command == null) {
            logger.warning("No Command Param");
            resp.setStatus(404);
            resp.getWriter().println("No command param found");
            return;
        }

        if ("buy".equalsIgnoreCase(command)) {
            try {
                jsonObject = new JSONObject(content);
                pair = jsonObject.getString("currencyPair");
                price = jsonObject.getLong("price");
                quantity = jsonObject.getLong("quantity");
                orderId = jsonObject.getLong("orderId");

            } catch (JSONException a) {
                logger.warning("JSON not well-formed");
                resp.setStatus(403);
                resp.getWriter().println("order post to VPS is not well-formed");
                return;
            }
            market.enterMarket(pair, orderId, Side.BUY, price, quantity);
            return;
        }
        if ("sell".equalsIgnoreCase(command)) {
            try {
                jsonObject = new JSONObject(content);
                pair = jsonObject.getString("currencyPair");
                price = jsonObject.getLong("price");
                quantity = jsonObject.getLong("quantity");
                orderId = jsonObject.getLong("orderId");

            } catch (JSONException a) {
                logger.warning("JSON not well-formed");
                resp.setStatus(403);
                resp.getWriter().println("order post to VPS is not well-formed");
                return;
            }
            market.enterMarket(pair, orderId, Side.SELL, price, quantity);
            return;
        }
        if ("cancelall".equalsIgnoreCase(command)) {
            try {
                jsonObject = new JSONObject(content);
                pair = jsonObject.getString("currencyPair");
                orderId = jsonObject.getLong("orderId");
                OrderBook orderBook = market.orderBooksList.get(pair);
                orderBook.cancelAll(orderId);
                resp.getWriter().println("Order " + orderId + " cancel successfully!");
                System.out.println("Order " + orderId + " cancel successfully!");
                resp.setStatus(200);
            } catch (JSONException a) {
                logger.warning("JSON not well-formed");
                resp.setStatus(403);
                resp.getWriter().println("order post to VPS is not well-formed");
                return;
            }
        }
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!market.isEmpty()) return;
        String rsp = Jsoup.connect("https://1-dot-bittrexcln.appspot.com/api/v1/backup/getorders").method(Connection.Method.GET).ignoreContentType(true).execute().body();
        JSONObject obj = new JSONObject(rsp);
        JSONArray buy = obj.getJSONArray("buy");
        JSONArray sell = obj.getJSONArray("sell");
        for (int i = 0; i < buy.length(); i++) {
            JSONObject order = buy.getJSONObject(i);
            String pair = order.getString("pair");
            long orderId = order.getLong("id");
            double price = order.getLong("price") * 1000_000_000_000L;
            double quantity = order.getLong("amount") * 1000_000_000_000L;
            logger.info("Buy Order " + i + " is: " + RESTUtil.gson.toJson(order));
            market.enterMarket(pair, orderId, Side.BUY, (long) price, (long) quantity);
        }
        for (int i = 0; i < sell.length(); i++) {
            JSONObject order = sell.getJSONObject(i);
            String pair = order.getString("pair");
            long orderId = order.getLong("id");
            double price = order.getLong("price") * 1000_000_000_000L;
            double quantity = order.getLong("amount") * 1000_000_000_000L;
            logger.info("Buy Order " + i + " is: " + RESTUtil.gson.toJson(order));
            market.enterMarket(pair, orderId, Side.SELL, (long) price, (long) quantity);
        }
        logger.info("Reinit Matching Engine done!");
    }
}
