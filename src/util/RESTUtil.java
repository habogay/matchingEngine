package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RESTUtil {
    public static Gson gson = new Gson();
    private static Logger LOGGER = Logger.getLogger(RESTUtil.class.getName());
    private static String receiverDir = "/geth/matchinglog.txt";

    public static String readStringInput(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        StringBuffer buffer = new StringBuffer();
        String line;
        while ((line = br.readLine()) != null) {
            buffer.append(line);
        }
        LOGGER.setLevel(Level.INFO);
        LOGGER.info("String: " + buffer.toString());
        return buffer.toString();
    }

    public static void writeToLog(String str) {
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        try {
            File file = new File(receiverDir);
            if (!file.exists()) file.createNewFile();
            fileWriter = new FileWriter(file, true);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(str);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) bufferedWriter.close();
                if (fileWriter != null) fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Exception at final");
            }
        }
    }

    public static double minusDouble(double a, double b) {
        double result = 0;
        BigDecimal subtrahend = BigDecimal.valueOf(a);
        BigDecimal minuend = BigDecimal.valueOf(b);
        result = subtrahend.subtract(minuend).doubleValue();
        return result;
    }

    public static double addDouble(double... numbers) {
        double result = 0;
        BigDecimal init = BigDecimal.valueOf(0);
        for (double a : numbers) {
            BigDecimal bd = BigDecimal.valueOf(a);
            init = init.add(bd);
        }
        result = init.doubleValue();
        return result;
    }

    public static double divideDouble(double num1, double num2) {
        BigDecimal bd1 = BigDecimal.valueOf(num1);
        BigDecimal bd2 = BigDecimal.valueOf(num2);
        return bd1.divide(bd2, 12, RoundingMode.HALF_UP).doubleValue();
    }

    public static double multipleDouble(double... numbers) {
        double result = 0;
        BigDecimal init = BigDecimal.valueOf(numbers[0]);
        for (int i = 1; i < numbers.length; i++) {
            BigDecimal bd = BigDecimal.valueOf(numbers[i]);
            init = init.multiply(bd);
        }
        result = init.doubleValue();
        return result;
    }


}
