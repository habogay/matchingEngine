package engine;

import com.google.gson.Gson;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectRBTreeMap;
import it.unimi.dsi.fastutil.longs.LongComparator;
import it.unimi.dsi.fastutil.longs.LongComparators;
import org.jsoup.Jsoup;
import util.RESTUtil;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.logging.Logger;

public class OrderBook {
    Logger logger = Logger.getLogger(OrderBook.class.getSimpleName());
    public String pricelevel_link = "https://1-dot-bittrexcln.appspot.com/api/v1/matching/pricelevel";
    Gson gson = new Gson();
    /**
     * ORDERBOOK CONTAIL ALL PRICELEVEL OF A PAIR.
     */
    public Long2ObjectRBTreeMap<PriceLevel> bids;
    public Long2ObjectRBTreeMap<PriceLevel> asks;
    public Long2ObjectOpenHashMap<Order> totalorders;
    public String pair = null;
    private OrderBookListener listener;
    double totalBid_coin0 = 0;
    double totalBid_coin1 = 0;
    double totalAsk_coin0 = 0;
    double totalAsk_coin1 = 0;

    public void setOrderBookListener(OrderBookListener listener) {
        this.listener = listener;
    }

    public OrderBook(String pair) {
        this.pair = pair;
        totalorders = new Long2ObjectOpenHashMap<>();
        bids = new Long2ObjectRBTreeMap<>(LongComparators.OPPOSITE_COMPARATOR);
        asks = new Long2ObjectRBTreeMap<>(LongComparators.NATURAL_COMPARATOR);
    }

    public void enterBook(long orderId, Side side, long price, long quantity) {
        if (totalorders.containsKey(orderId)) return;
        if (Side.BUY == side) {
            matchToAsks(orderId, price, quantity);
            return;
        }
        if (Side.SELL == side) {
            matchToBids(orderId, price, quantity);
            return;
        }
    }

    private void matchToAsks(long orderId, long price, long quantity) {
        PriceLevel priceLevel = getTopLevel(asks);
        double oldQuantityDbl = RESTUtil.divideDouble((double) quantity, 1000_000_000_000D);
        while (priceLevel != null && quantity > 0 && priceLevel.getPrice() <= price) {
            logger.warning("Vao Vong lap: Quantity: " + quantity);
            quantity = priceLevel.match(orderId, Side.BUY, quantity, listener);
            double newQuantityDbl = RESTUtil.divideDouble(quantity, 1000_000_000_000D);
            double priceDbl = RESTUtil.divideDouble(priceLevel.getPrice(), 1000_000_000_000D);
            double quantityChange = RESTUtil.minusDouble(oldQuantityDbl, newQuantityDbl);
            totalAsk_coin1 = RESTUtil.minusDouble(totalAsk_coin1, quantityChange);
            totalAsk_coin0 = RESTUtil.minusDouble(totalAsk_coin0, RESTUtil.multipleDouble(quantityChange, priceDbl));

            HashMap<String, Object> data = new HashMap<>();
            try {
                Thread.sleep(100);
                data.put("price", priceLevel.getPrice());
                data.put("side", priceLevel.getSide());
                data.put("type", "minus");
                data.put("totalQuantity", priceLevel.getTotalQuantity());
                data.put("itemCount", priceLevel.ordersList.size());
                data.put("pair", pair);
                data.put("totalAsk_coin0", totalAsk_coin0);
                data.put("totalAsk_coin1", totalAsk_coin1);
                data.put("totalBid_coin0", totalBid_coin0);
                data.put("totalBid_coin1", totalBid_coin1);
                Jsoup.connect(pricelevel_link).timeout(0).requestBody(gson.toJson(data)).post();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            oldQuantityDbl = newQuantityDbl;
            if (priceLevel.isEmpty()) {
                asks.remove(priceLevel.getPrice());
                priceLevel = getTopLevel(asks);
            }
        }
        logger.info("Quantity in matchToAsk: " + quantity);
        if (quantity > 0) {
            totalorders.put(orderId, add(bids, orderId, Side.BUY, price, quantity));
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                System.out.println("Interrupted");
//            }
            listener.add(orderId, Side.BUY, price, quantity);

        }

    }

    private void matchToBids(long orderId, long price, long quantity) {
        PriceLevel priceLevel = getTopLevel(bids);
        double oldQuantityDbl = RESTUtil.divideDouble((double) quantity, 1000_000_000_000D);
        while (priceLevel != null && quantity > 0 && price <= priceLevel.getPrice()) {
            logger.warning("Vao Vong lap: Quantity: " + quantity);
            quantity = priceLevel.match(orderId, Side.SELL, quantity, listener);
            double newQuantityDbl = RESTUtil.divideDouble(quantity, 1000_000_000_000D);
            double priceDbl = RESTUtil.divideDouble(priceLevel.getPrice(), 1000_000_000_000D);
            double quantityChange = RESTUtil.minusDouble(oldQuantityDbl, newQuantityDbl);
            totalBid_coin1 = RESTUtil.minusDouble(totalBid_coin1, quantityChange);
            totalBid_coin0 = RESTUtil.minusDouble(totalBid_coin0, RESTUtil.multipleDouble(quantityChange, priceDbl));

            HashMap<String, Object> data = new HashMap<>();
            try {
                Thread.sleep(100);
                data.put("price", priceLevel.getPrice());
                data.put("side", priceLevel.getSide());
                data.put("type", "minus");
                data.put("totalQuantity", priceLevel.getTotalQuantity());
                data.put("itemCount", priceLevel.ordersList.size());
                data.put("pair", pair);
                data.put("totalAsk_coin0", totalAsk_coin0);
                data.put("totalAsk_coin1", totalAsk_coin1);
                data.put("totalBid_coin0", totalBid_coin0);
                data.put("totalBid_coin1", totalBid_coin1);
                Jsoup.connect(pricelevel_link).timeout(0).requestBody(gson.toJson(data)).post();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            oldQuantityDbl = newQuantityDbl;
            if (priceLevel.isEmpty()) {
                bids.remove(priceLevel.getPrice());
                priceLevel = getTopLevel(bids);
                logger.warning("Lay Price Level moi thanh cong!");

            } else {
                logger.warning("Khong lay price levelmoi");
            }
        }
        if (quantity > 0) {
            totalorders.put(orderId, add(asks, orderId, Side.SELL, price, quantity));
            System.out.println("order sell added: " + orderId + "---" + Side.SELL + "---" + price + "---" + quantity);
//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                System.out.println("Interrupted");
//            }
            listener.add(orderId, Side.SELL, price, quantity);

        }
    }

    public Order add(Long2ObjectRBTreeMap<PriceLevel> pricelevels, long orderId, Side side, long price, long quantity) {
        PriceLevel priceLevel = pricelevels.get(price);
        if (priceLevel == null) {
            priceLevel = new PriceLevel(side, price, pair);
            pricelevels.put(price, priceLevel);
        }

        Order orderadded = priceLevel.add(orderId, quantity);
        priceLevel.setTotalQuantity(priceLevel.getTotalQuantity() + quantity);
        double quantityDbl = RESTUtil.divideDouble((double) quantity, 1000_000_000_000D);
        double priceDbl = RESTUtil.divideDouble((double) price, 1000_000_000_000D);
        if (Side.BUY == side) {
            totalBid_coin1 = RESTUtil.addDouble(totalBid_coin1, quantityDbl);
            totalBid_coin0 = RESTUtil.addDouble(totalBid_coin0, RESTUtil.multipleDouble(quantityDbl, priceDbl));
        }
        if (Side.SELL == side) {
            totalAsk_coin1 = RESTUtil.addDouble(totalAsk_coin1, quantityDbl);
            totalAsk_coin0 = RESTUtil.addDouble(totalAsk_coin0, RESTUtil.multipleDouble(quantityDbl, priceDbl));
        }

        HashMap<String, Object> data = new HashMap<>();
        try {
            Thread.sleep(50);
            data.put("price", priceLevel.getPrice());
            data.put("side", priceLevel.getSide());
            data.put("type", "add");
            data.put("amount", quantity);
            data.put("itemCount", priceLevel.ordersList.size());
            data.put("pair", pair);
            data.put("totalQuantity", priceLevel.getTotalQuantity());
            data.put("totalAsk_coin0", totalAsk_coin0);
            data.put("totalAsk_coin1", totalAsk_coin1);
            data.put("totalBid_coin0", totalBid_coin0);
            data.put("totalBid_coin1", totalBid_coin1);
            Jsoup.connect(pricelevel_link).timeout(0).requestBody(gson.toJson(data)).post();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Total size of " + price + " : " + priceLevel.ordersList.size() + " total: " + priceLevel.countTotalQuantity());
        return orderadded;
    }

    /**
     * THE INCOMING ORDER WITH BE COMPARED AND MATCHED TO THE EXISTING ORDERS BEFORE ADDED TO LIST ORDERS.
     */
    public PriceLevel getTopLevel(Long2ObjectRBTreeMap<PriceLevel> pricelevels) {
        if (pricelevels.isEmpty()) return null;
        return pricelevels.get(pricelevels.firstLongKey());
    }

    public void cancelAll(long orderId) {
        Order order = totalorders.get(orderId);
        if (order == null) return;
        totalorders.remove(orderId);
        logger.info("totalorders Size: " + totalorders.size() + " contain " + orderId + " :" + totalorders.containsKey(orderId));
        PriceLevel level = order.getPriceLevel();
        level.remove(order);
        double quantityDbl = RESTUtil.divideDouble((double) order.getQuantity(), 1000_000_000_000D);
        double priceDbl = RESTUtil.divideDouble((double) level.getPrice(), 1000_000_000_000D);
        if (Side.BUY == level.getSide()) {
            totalBid_coin1 = RESTUtil.minusDouble(totalBid_coin1, quantityDbl);
            totalBid_coin0 = RESTUtil.minusDouble(totalBid_coin0, RESTUtil.multipleDouble(quantityDbl, priceDbl));
        }
        if (Side.SELL == level.getSide()) {
            totalAsk_coin1 = RESTUtil.minusDouble(totalAsk_coin1, quantityDbl);
            totalAsk_coin0 = RESTUtil.minusDouble(totalAsk_coin0, RESTUtil.multipleDouble(quantityDbl, priceDbl));
        }
        HashMap<String, Object> data = new HashMap<>();
        try {
            Thread.sleep(50);
            data.put("price", level.getPrice());
            data.put("side", level.getSide());
            data.put("type", "remove");
            data.put("amount", order.getQuantity());
            data.put("itemCount", level.ordersList.size());
            data.put("pair", pair);
            data.put("totalQuantity", level.getTotalQuantity());
            data.put("totalAsk_coin0", totalAsk_coin0);
            data.put("totalAsk_coin1", totalAsk_coin1);
            data.put("totalBid_coin0", totalBid_coin0);
            data.put("totalBid_coin1", totalBid_coin1);
            Jsoup.connect(pricelevel_link).timeout(0).ignoreContentType(true).requestBody(gson.toJson(data)).post();
            System.out.println("SEND CANCEL ORDER TO GAE!" + gson.toJson(data));
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if (level.isEmpty()) {
            delete(level);
        }
        listener.cancelAll(orderId);
    }

    private void delete(PriceLevel level) {
        switch (level.getSide()) {
            case BUY:
                bids.remove(level.getPrice());
                break;
            case SELL:
                asks.remove(level.getPrice());
                break;
        }
    }

}


