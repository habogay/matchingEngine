package engine;

public class Order {
    private PriceLevel priceLevel;
    private long id;
    private long quantity;

    public Order() {
    }

    public Order(PriceLevel priceLevel, long id, long quantity) {
        this.priceLevel = priceLevel;
        this.id = id;
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public PriceLevel getPriceLevel() {
        return priceLevel;
    }

    public long getQuantity() {
        return quantity;
    }

    public void reduce(long quantity) {
        this.quantity -= quantity;
    }

    public void resize(long size) {
        this.quantity = size;
    }
}

