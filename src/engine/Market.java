package engine;

import com.google.gson.Gson;
import org.jsoup.Jsoup;
import util.RESTUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Logger;

public class Market implements OrderBookListener {
    private String log_link = "https://1-dot-bittrexcln.appspot.com/api/v1/matching/";
    public HashMap<String, OrderBook> orderBooksList = new HashMap<>();


    Logger logger = Logger.getLogger(Market.class.getSimpleName());

    /**
     * OPEN ORDER BOOK INCASE THE PAIR DOES NOT EXIST
     */
    public OrderBook open(String pair) {
        OrderBook orderBook = orderBooksList.get(pair);
        if (orderBook == null) {
            orderBook = new OrderBook(pair);
            orderBook.setOrderBookListener(this);
            orderBooksList.put(pair, orderBook);

        }
        return orderBook;
    }

    public void enterMarket(String pair, long orderId, Side side, long price, long quantity) {
        OrderBook orderBook = orderBooksList.get(pair);
        if (orderBook == null) orderBook = open(pair);
        orderBook.enterBook(orderId, side, price, quantity);

    }

    @Override
    public void match(long waitingOrderId, long incomingOrderId, Side incomingSide, long price, long executedQuantity, long remainingQuantity) {
        long a = 1000000000000000000L;
        HashMap<String, Object> content = new HashMap<>();
        content.put("waitingOrderId", waitingOrderId);
        content.put("incomingOrderId", incomingOrderId);
        content.put("incomingSide", incomingSide);
        content.put("price", price);
        content.put("executedQuantity", executedQuantity);
        content.put("remainingQuantity", remainingQuantity);
        content.put("command", "match");
        System.out.println("matched: " + RESTUtil.gson.toJson(content));
        logger.info("matched: " + RESTUtil.gson.toJson(content));
        try {
            Jsoup.connect(log_link).timeout(0).requestBody(new Gson().toJson(content)).post();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void add(long orderId, Side side, long price, long quantity) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("orderId", orderId);
        content.put("side", side);
        content.put("price", price);
        content.put("quantity", quantity);
        content.put("command", "add");
        System.out.println("added: " + RESTUtil.gson.toJson(content));
        logger.info("added: " + RESTUtil.gson.toJson(content));
        try {
            Jsoup.connect(log_link).timeout(0).requestBody(new Gson().toJson(content)).post();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void cancel(long orderId, long canceledQuantity, long remainingQuantity) {
        HashMap<String, Object> content = new HashMap<>();
        content.put("orderId", orderId);
        content.put("canceledQuantity", canceledQuantity);
        content.put("remainingQuantity", remainingQuantity);
        content.put("command", "cancel");
        System.out.println("cancel: " + RESTUtil.gson.toJson(content));
        logger.info("cancel: " + RESTUtil.gson.toJson(content));
        try {
            Jsoup.connect(log_link).timeout(0).requestBody(new Gson().toJson(content)).post();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void cancelAll(long orderId) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("orderId", orderId);
        map.put("command", "cancelAll");
        String content = RESTUtil.gson.toJson(map);
        try {
            Jsoup.connect(log_link).timeout(0).requestBody(content).post();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isEmpty() {
        return orderBooksList.isEmpty();
    }
}
