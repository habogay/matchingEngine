package engine;

public interface OrderBookListener {
    /**
     * Match the price of incoming order to the exist orders. Matching occurs when price ask <= price bid
     * @param waitingOrderId the order identifier of the resting order
     * @param incomingOrderId the order identifier of the incoming order
     * @param incomingSide the side of the incoming order
     * @param price the execution price
     * @param executedQuantity the executed quantity
     * @param remainingQuantity the remaining quantity of the resting order
     *
     * */
    void match(long waitingOrderId,long incomingOrderId,Side incomingSide,long price,long executedQuantity,long remainingQuantity);
    /**
     * Add an order to the order book.
     *
     * @param orderId the order identifier
     * @param side the side
     * @param price the limit price
     * @param quantity the size
     */
    void add(long orderId,Side side,long price, long quantity);
    /**
     * Cancel a quantity of an order.
     *
     * @param orderId the order identifier
     * @param canceledQuantity the canceled quantity
     * @param remainingQuantity the remaining quantity
     */
    void cancel(long orderId,long canceledQuantity,long remainingQuantity);
    void cancelAll(long orderId);
}
